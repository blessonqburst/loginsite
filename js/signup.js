
function validatesignup() {
  var fname = document.getElementById("fname").value;
  var lname = document.getElementById("lname").value;
  var uname = document.getElementById("uname").value;
  var email = document.getElementById("email").value;
  var psw   = document.getElementById("psw").value;
  
  var atpos = email.indexOf("@");
  var dotpos= email.lastIndexOf(".");

  var txt   = {"employee":[{"firstName":fname, "lastName":lname, "username":uname, "email":email, "password":psw}]}

  document.getElementById("display1").style.display="none";
  document.getElementById("display2").style.display="none";
  document.getElementById("display3").style.display="none";

  if(uname=="")
  {
    document.getElementById("display1").style.display="block";
    document.getElementById("display1").innerHTML = "Please enter a username";
    document.getElementById("button1").disabled = false;
    return false;
  }

  else if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length){
    document.getElementById("display2").style.display="block";
    document.getElementById("display2").innerHTML = "Please enter a valid e-mail address";
    document.getElementById("button1").disabled = false;
    return false;
  }

  else if (psw.length < 6) {
    document.getElementById("display3").style.display="block";
    document.getElementById("display3").innerHTML = "Password too short.";
    document.getElementById("button1").disabled = false;
    return false;
  }
  
  else {
    var cnt=localStorage.length;
    var i;
    for(i=0;i<cnt;i++) {
      var keyvalue="userdetails"+i.toString();
      var accdet = localStorage.getItem(keyvalue);
      account = JSON.parse(accdet);

      if(uname == account.employee[0].username){
        document.getElementById("display1").style.display="block";
        document.getElementById("display1").innerHTML = "Username already registered";
        document.getElementById("button1").disabled = false;
        return false;
      }

      else if(email == account.employee[0].email){
        document.getElementById("display2").style.display="block";
        document.getElementById("display2").innerHTML = "Email-id is already registered";
        document.getElementById("button1").disabled = false;
        return false;
      }

    }
    var count=cnt.toString();
    var i="userdetails"+count;
    localStorage.setItem(i, JSON.stringify(txt));

    window.location = "signupsuccess.html";
    return false;
  }
}
